##How to compile & run your application

(Project targets framework .net core 2.1) 

Navigate to project folder "\BookingApp.Hotels.Api" 

Run command 'dotnet run' 

##Examples of the REST calls

Swagger can tell you all you need, find the API documentation at  ~/swagger  

##How to run the test cases

Easiest way to run tests is to use test runner in visual studio solution, The test project is 'BookingApp.Hotels.Test.Unit' 

##Mention anything that was asked but not delivered and why

No authentication due to time contraints. Also the search is more of a find by name function as I was running out of time.

Please see other notes below in additonal comments. 

##Any additional comments


Due to time constrains I focused more on the core application - showing some patterns about how I like to structure an application. So the applciation level API could do with some more work - validation (api contract models) could improve, use automapper, etc. Also room images aren't being stored in database for the moment, using EF and some json translation would be needed here. I thought decoupling patterns more important to show than some of these details.

I found some of the relationships a bit difficult to establish from reading the business requirements, partilcuarly regarding room management. Therefore I have made some assumptions to complete the task, but these would really need discussion and clarification.

From the spec, the hotel API seems to manage the availabile rooms avaulabiliy dates. With this assumption, when a booking is created the API consumer application will need to manage that room's available dates after the booking is made.

From the spec, the hotel API seems to manage the availabile rooms. With this assumption, the rooms are volitle (so can be deleted/changed) so the hard relationship cannot be established between room and a booking. The booking therefore references the hotel, with some refernce to room type (description) at time of booking. The current room constrains are also validated at time of creating the booking.

