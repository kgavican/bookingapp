﻿using System;
using System.Collections.Generic;
using BookingApp.Hotels.AppCore.Entities;
using Xunit;

namespace BookingApp.Tests.Unit.AppCore.Entities
{
    public class RoomTests
    {
        [Fact]
        public void RoomCannotBeCreatedWithInvalidStartDate()
        {
            // Given a room with end date before start date
            var startDate = new DateTime(2018, 02, 01);
            var endDate = new DateTime(2018, 01, 01);

            // When I create the object
            // Then I get an invalid argument exception
            Assert.Throws<ArgumentException>(() => new Room("Test Room", new List<string>(),
                startDate,
                endDate,
                2));
        }


        [Fact]
        public void RoomMustHaveDescription()
        {
            // Given a room with end date before start date
            var startDate = new DateTime(2018, 01, 01);
            var endDate = new DateTime(2018, 02, 01);

            // When I create the object
            // Then I get an invalid argument exception
            Assert.Throws<ArgumentNullException>(() => new Room(string.Empty, new List<string>() { "http://testimage.jpg" },
                startDate,
                endDate,
                2));
        }

        [Fact]
        public void RoomMustHaveImages()
        {
            // Given a room with end date before start date
            var startDate = new DateTime(2018, 01, 01);
            var endDate = new DateTime(2018, 02, 01);

            // When I create the object
            // Then I get an invalid argument exception
            Assert.Throws<ArgumentException>(() => new Room(string.Empty, new List<string>(),
                startDate,
                endDate,
                2));
        }

        [Fact]
        public void ValidRoomIsCreated()
        {
            // Given a room with end date before start date
            var startDate = new DateTime(2018, 01, 01);
            var endDate = new DateTime(2018, 02, 01);

            // When I create the object
            var room = new Room("Test Room", new List<string>() { "http://testimage.jpg" },
                startDate,
                endDate,
                2);

            // Then values are as expected
            Assert.Equal("Test Room", room.Description);
            Assert.Equal(startDate, room.AvailableStartDate);
            Assert.Equal(endDate, room.AvailableEndDate);
            Assert.Equal(2, room.MaxGuests);
        }

        [Fact]
        public void RoomDateCheckSucceedsForValidDates()
        {
            // Given a room available for Jan
            var startDate = new DateTime(2018, 01, 01);
            var endDate = new DateTime(2018, 02, 01);

            var room = new Room("Test Room", new List<string>() { "http://testimage.jpg" },
                startDate,
                endDate,
                2);

            // When I check the availability for January dates
            // Then the room should be available

            Assert.True(room.IsAvailableForDates(new DateTime(2018, 01, 01), new DateTime(2018, 01, 31)));
            Assert.True(room.IsAvailableForDates(new DateTime(2018, 01, 10), new DateTime(2018, 01, 17)));
            Assert.True(room.IsAvailableForDates(new DateTime(2018, 01, 31), new DateTime(2018, 02, 01)));
        }

        [Fact]
        public void RoomDateCheckFailsorInvalidDates()
        {
            // Given a room available for Jan
            var startDate = new DateTime(2018, 01, 01);
            var endDate = new DateTime(2018, 02, 01);

            var room = new Room("Test Room", new List<string>() { "http://testimage.jpg" },
                startDate,
                endDate,
                2);

            // When I check the availability for Feb dates
            // Then the room should not be available

            Assert.False(room.IsAvailableForDates(new DateTime(2018, 02, 01), new DateTime(2018, 02, 03)));
            Assert.False(room.IsAvailableForDates(new DateTime(2018, 01, 31), new DateTime(2018, 02, 02)));
            Assert.False(room.IsAvailableForDates(new DateTime(2017, 12, 31), new DateTime(2018, 01, 01)));
        }
    }
}
