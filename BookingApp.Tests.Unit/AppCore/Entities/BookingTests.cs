﻿using System;
using System.Collections.Generic;
using BookingApp.Hotels.AppCore.Entities;
using Xunit;

namespace BookingApp.Tests.Unit.AppCore.Entities
{
    public class BookingTests
    {
        [Fact]
        public void BookingCannotBeCreatedWithInvalidDates()
        {
            // Given a room with end date before start date
            var startDate = new DateTime(2018, 02, 01);
            var endDate = new DateTime(2018, 01, 01);

            Assert.Throws<ArgumentException>(() => new Booking(1, "Double room", startDate, endDate, new List<Guest>()
            {
                new Guest("John Smith", 18)
            }));
        }

        [Fact]
        public void BookingCannotBeCreatedWithoutGuest()
        {
            // Given a room with end date before start date
            var startDate = new DateTime(2018, 01, 01);
            var endDate = new DateTime(2018, 02, 01);

            Assert.Throws<ArgumentException>(() => new Booking(1, "Double room", startDate, endDate, new List<Guest>()));
        }

        [Fact]
        public void BookingCannotBeCreatedWithoutRoomDescription()
        {
            // Given a room with end date before start date
            var startDate = new DateTime(2018, 01, 01);
            var endDate = new DateTime(2018, 02, 01);

            Assert.Throws<ArgumentNullException>(() => new Booking(1, "", startDate, endDate, new List<Guest>()
            {
                new Guest("John Smith", 18)
            }));
        }
    }
}
