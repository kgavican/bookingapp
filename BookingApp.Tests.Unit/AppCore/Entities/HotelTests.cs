﻿using System;
using BookingApp.Hotels.AppCore.Entities;
using Xunit;

namespace BookingApp.Tests.Unit.AppCore.Entities
{
    public class HotelTests
    {
        [Fact]
        public void HotelMustHaveName()
        {
            Assert.Throws<ArgumentNullException>(() => new Hotel(
                "",
                "12 Grafton Street",
                "Dublin",
                "+35318224478",
                5));
        }

        [Fact]
        public void HotelMustHaveAddress()
        {
            Assert.Throws<ArgumentNullException>(() => new Hotel(
                "Test Hotel 1",
                "",
                "Dublin",
                "+35318224478",
                5));
        }

        [Fact]
        public void HotelMustHaveCity()
        {
            Assert.Throws<ArgumentNullException>(() => new Hotel(
                "Test Hotel 1",
                "12 Grafton Street",
                "",
                "+35318224478",
                5));
        }

        [Fact]
        public void HotelMustHavePhone()
        {
            Assert.Throws<ArgumentNullException>(() => new Hotel(
                "Test Hotel 1",
                "12 Grafton Street",
                "Dublin",
                "",
                5));
        }
    }
}
