using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookingApp.Hotels.AppCore.Entities;
using BookingApp.Hotels.AppCore.Exceptions;
using BookingApp.Hotels.AppCore.Interfaces;
using BookingApp.Hotels.AppCore.Models;
using BookingApp.Hotels.AppCore.Services;
using Moq;
using Xunit;

namespace BookingApp.Tests.Unit.AppCore.Services
{
    public class HotelServiceTests
    {
        [Fact]
        public async Task HotelCannotBeDeletedIfThereIsAvailableRooms()
        {
            // Given a hotel with available rooms

            var hotelRepo = new Mock<IHotelRepository>();

            var hotelWithAvailableRooms = new Hotel(
                "Test Hotel 1",
                "12 Grafton Street",
                "Dublin",
                "+35318224478",
                5,
                new List<Room>()
                {
                    new Room("Couples Room", new List<string>()
                        {
                            "http://somehost/someimage.jpg"
                        },
                        new DateTime(2018, 07, 01),
                        new DateTime(2018, 07, 08),
                        2 ),
                    new Room("Family Room", new List<string>()
                        {
                            "http://somehost/someimage.jpg"
                        },
                        new DateTime(2018, 07, 04),
                        new DateTime(2018, 07, 07),
                        5 ),
                });

            hotelRepo.Setup(i => i.GetHotelWithAvailableRoomsAndBookings(5)).ReturnsAsync(hotelWithAvailableRooms);
            
            var hotelService = new HotelService(hotelRepo.Object,
                new Mock<IAsyncRepository<Booking>>().Object, new Mock<IAsyncRepository<Room>>().Object);


            // When I try delete
            // Then error should be thrown
            await Assert.ThrowsAsync<HotelCannotDeleteException>(() => hotelService.DeleteAsync(5));

            // And it should not be deleted
            hotelRepo.Verify(i => i.DeleteAsync(It.IsAny<Hotel>()), Times.Never);
        }

        [Fact]
        public async Task HotelCannotBeDeletedIfThereIsValidReservations()
        {
            // Given a hotel with reservations
            var hotelRepo = new Mock<IHotelRepository>();

            var hotelWithBookings = new Hotel(
                "Test Hotel 2",
                "13 Grafton Street",
                "Dublin",
                "+35318224479",
                5);

            hotelWithBookings.Bookings.Add(
                new Booking(6, "Double room",
                    new DateTime(2018, 07, 10),
                    new DateTime(2018, 07, 12),
                    new List<Guest>()
                    {
                        new Guest("John Smith", 30),
                        new Guest("James Smith", 30)
                    }));

            hotelRepo.Setup(i => i.GetHotelWithAvailableRoomsAndBookings(6)).ReturnsAsync(hotelWithBookings);

            var hotelService = new HotelService(hotelRepo.Object,
                new Mock<IAsyncRepository<Booking>>().Object, new Mock<IAsyncRepository<Room>>().Object);

            // When I try delete
            // Then an error should be thrown
            await Assert.ThrowsAsync<HotelCannotDeleteException>(() => hotelService.DeleteAsync(6));

            // And it should not be deleted
            hotelRepo.Verify(i => i.DeleteAsync(It.IsAny<Hotel>()), Times.Never);
        }

        [Fact]
        public async Task HotelCanBeDeletedIfThereIsNoValidReservationsAndNoAvailableRooms()
        {
            // Given a hotel with no reservations or available rooms
            var hotelRepo = new Mock<IHotelRepository>();

            var hotelWithNone = new Hotel(
                "Test Hotel 3",
                "14 Grafton Street",
                "Dublin",
                "+35318224480",
                5,
                null);

            hotelRepo.Setup(i => i.GetHotelWithAvailableRoomsAndBookings(7)).ReturnsAsync(hotelWithNone);

            var hotelService = new HotelService(hotelRepo.Object,
                new Mock<IAsyncRepository<Booking>>().Object, new Mock<IAsyncRepository<Room>>().Object);

            // When I try delete it
            await hotelService.DeleteAsync(7);

            // It should be deleted
            hotelRepo.Verify(i => i.DeleteAsync(It.IsAny<Hotel>()), Times.Once);
        }

        [Fact]
        public async Task DeleteForUnknownHotelThrowsException()
        {
            // Given an unknow hotel id
            var hotelRepo = new Mock<IHotelRepository>();

            var hotelService = new HotelService(hotelRepo.Object,
                new Mock<IAsyncRepository<Booking>>().Object, new Mock<IAsyncRepository<Room>>().Object);

            // When I try delete
            // Then it should throw an error
            await Assert.ThrowsAsync<HotelNotFoundException>(() => hotelService.DeleteAsync(100));

            // And not be deleted
            hotelRepo.Verify(i => i.DeleteAsync(It.IsAny<Hotel>()), Times.Never);
        }

        [Fact]
        public async Task CannotAddBookingForUnknownRoom()
        {
            var bookingRepo = new Mock<IAsyncRepository<Booking>>();

            // Given booking for an unknown room id
            var hotelService = new HotelService(new Mock<IHotelRepository>().Object,
                bookingRepo.Object, new Mock<IAsyncRepository<Room>>().Object);

            var newBooking = new BookingRequest(10,
                new DateTime(2018, 1, 1),
                new DateTime(2018, 1, 2),
                new List<Guest>()
                {
                    new Guest("John Smith", 18)
                });

            // When I try add the booking
            // Then it should thrown an error
            await Assert.ThrowsAsync<RoomNotFoundException>(() => hotelService.AddBookingAsync(newBooking));

            // And not be booked
            bookingRepo.Verify(i => i.AddAsync(It.IsAny<Booking>()), Times.Never);
        }

        [Fact]
        public async Task CannotAddBookingForTooManyGuests()
        {
            var bookingRepo = new Mock<IAsyncRepository<Booking>>();

            // Given a room for 2 people
            var roomRepo = new Mock<IAsyncRepository<Room>>();

            var doubleRoom = new Room("Double Room", new List<string>()
                {
                    "http://somehost/someimage.jpg"
                },
                new DateTime(2018, 07, 01),
                new DateTime(2018, 07, 08),
                2);

            roomRepo.Setup(i => i.GetByIdAsync(It.IsAny<int>())).ReturnsAsync(doubleRoom);


            var hotelService = new HotelService(new Mock<IHotelRepository>().Object,
                new Mock<IAsyncRepository<Booking>>().Object, roomRepo.Object);

            // When I try book the room for 3
            var newBooking = new BookingRequest(10,
                new DateTime(2018, 7, 02),
                new DateTime(2018, 7, 05),
                new List<Guest>()
                {
                    new Guest("John Smith", 18),
                    new Guest("James Smith", 18),
                    new Guest("Jimmy Smith", 18)
                });

            // Then it should throw an error
            await Assert.ThrowsAsync<BookingInvalidException>(() => hotelService.AddBookingAsync(newBooking));

            // And not be booked
            bookingRepo.Verify(i => i.AddAsync(It.IsAny<Booking>()), Times.Never);
        }

        [Fact]
        public async Task CannotAddBookingForUnavailableDates()
        {
            // Given a room available for some dates
            var roomRepo = new Mock<IAsyncRepository<Room>>();
            var bookingRepo = new Mock<IAsyncRepository<Booking>>();

            var doubleRoom = new Room("Double Room", new List<string>()
                {
                    "http://somehost/someimage.jpg"
                },
                new DateTime(2018, 07, 01),
                new DateTime(2018, 07, 08),
                2);

            roomRepo.Setup(i => i.GetByIdAsync(It.IsAny<int>())).ReturnsAsync(doubleRoom);


            var hotelService = new HotelService(new Mock<IHotelRepository>().Object,
                bookingRepo.Object, roomRepo.Object);

            // Then I try book room outside of these dataes
            var newBooking = new BookingRequest(10,
                new DateTime(2018, 07, 07),
                new DateTime(2018, 07, 09),
                new List<Guest>()
                {
                    new Guest("John Smith", 18),
                    new Guest("James Smith", 18)
                });

            // Then it should throw an error
            await Assert.ThrowsAsync<BookingInvalidException>(() => hotelService.AddBookingAsync(newBooking));

            // And not be booked
            bookingRepo.Verify(i => i.AddAsync(It.IsAny<Booking>()), Times.Never);
        }


        [Fact]
        public async Task AvailableRoomsAreFoundForHotelNameAndDates()
        {
            // Given a hotel with available rooms

            var desiredHotelName = "test hotel";
            var desiredStartDate = new DateTime(2018, 07, 10);
            var desiredEndDate = new DateTime(2018, 07, 14);

            var hotelRepo = new Mock<IHotelRepository>();

            var hotelWithAvailableRooms = new Hotel(
                "Test Hotel",
                "12 Grafton Street",
                "Dublin",
                "+35318224478",
                5,
                new List<Room>()
                {
                    new Room("Couples Room", new List<string>()
                        {
                            "http://somehost/someimage.jpg"
                        },
                        new DateTime(2018, 07, 01),
                        new DateTime(2018, 07, 08),
                        2 ),
                    new Room("Family Room", new List<string>()
                        {
                            "http://somehost/someimage.jpg"
                        },
                        new DateTime(2018, 07, 08),
                        new DateTime(2018, 07, 15),
                        5 ),
                    new Room("Duluxe Room", new List<string>()
                        {
                            "http://somehost/someimage.jpg"
                        },
                        new DateTime(2018, 07, 10),
                        new DateTime(2018, 07, 22),
                        3 ),
                });

            hotelRepo.Setup(i => i.GetHotelByNameAsync(desiredHotelName)).ReturnsAsync(hotelWithAvailableRooms);

            var hotelService = new HotelService(hotelRepo.Object,
                new Mock<IAsyncRepository<Booking>>().Object, new Mock<IAsyncRepository<Room>>().Object);


            var result =
                (await hotelService.SearchForAvailableRoomsAsync("test hotel", desiredStartDate, desiredEndDate)).ToList();

            Assert.Equal(2, result.Count);

            Assert.Contains("Family Room", result.Select(i => i.Description));
            Assert.Contains("Duluxe Room", result.Select(i => i.Description));
            Assert.DoesNotContain("Couples Room", result.Select(i => i.Description));
        }


        [Fact]
        public async Task AvailableRoomsAreNotReturnedForHotelNameWhenNoMatchingDates()
        {
            // Given a hotel with available rooms

            var desiredHotelName = "test hotel";
            var desiredStartDate = new DateTime(2018, 06, 30);
            var desiredEndDate = new DateTime(2018, 07, 02);

            var hotelRepo = new Mock<IHotelRepository>();

            var hotelWithAvailableRooms = new Hotel(
                "Test Hotel",
                "12 Grafton Street",
                "Dublin",
                "+35318224478",
                5,
                new List<Room>()
                {
                    new Room("Couples Room", new List<string>()
                        {
                            "http://somehost/someimage.jpg"
                        },
                        new DateTime(2018, 07, 01),
                        new DateTime(2018, 07, 08),
                        2 ),
                    new Room("Family Room", new List<string>()
                        {
                            "http://somehost/someimage.jpg"
                        },
                        new DateTime(2018, 07, 08),
                        new DateTime(2018, 07, 15),
                        5 ),
                    new Room("Duluxe Room", new List<string>()
                        {
                            "http://somehost/someimage.jpg"
                        },
                        new DateTime(2018, 07, 10),
                        new DateTime(2018, 07, 22),
                        3 ),
                });

            hotelRepo.Setup(i => i.GetHotelByNameAsync(desiredHotelName)).ReturnsAsync(hotelWithAvailableRooms);

            var hotelService = new HotelService(hotelRepo.Object,
                new Mock<IAsyncRepository<Booking>>().Object, new Mock<IAsyncRepository<Room>>().Object);

            var result =
                (await hotelService.SearchForAvailableRoomsAsync("test hotel", desiredStartDate, desiredEndDate)).ToList();

            Assert.Empty(result);
        }

    }
}
