﻿using System;

namespace BookingApp.Hotels.AppCore.Exceptions
{
    public class RoomNotFoundException : Exception
    {
    }
}
