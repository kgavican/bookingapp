﻿using System;

namespace BookingApp.Hotels.AppCore.Exceptions
{
    public class BookingInvalidException : Exception
    {
        public BookingInvalidException(string message) : base(message)
        {
        }
    }
}
