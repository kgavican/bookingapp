﻿using System;

namespace BookingApp.Hotels.AppCore.Exceptions
{
    public class HotelCannotDeleteException : Exception
    {
        public HotelCannotDeleteException(string message) : base(message)
        {
        }
    }
}
