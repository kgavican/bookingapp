﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookingApp.Hotels.AppCore.Entities;
using BookingApp.Hotels.AppCore.Exceptions;
using BookingApp.Hotels.AppCore.Interfaces;
using BookingApp.Hotels.AppCore.Models;

namespace BookingApp.Hotels.AppCore.Services
{
    public class HotelService : IHotelService
    {
        private readonly IHotelRepository _hotelRepository;
        private readonly IAsyncRepository<Booking> _bookingRepository;
        private readonly IAsyncRepository<Room> _roomRepository;

        public HotelService(IHotelRepository hotelRepository, 
            IAsyncRepository<Booking> bookingRepository,
            IAsyncRepository<Room> roomRepository)
        {
            _hotelRepository = hotelRepository;
            _bookingRepository = bookingRepository;
            _roomRepository = roomRepository;
        }

        public async Task<Hotel> GetAsync(int id, bool includeAvailableRooms = false)
        {
            return includeAvailableRooms
                ? await _hotelRepository.GetHotelWithAvailableRooms(id)
                : await _hotelRepository.GetByIdAsync(id);
        }

        public async Task DeleteAsync(int id)
        {
            var hotel = await  _hotelRepository.GetHotelWithAvailableRoomsAndBookings(id);

            if (hotel == null) throw new HotelNotFoundException();

            if (hotel.AvailableRooms != null && hotel.AvailableRooms.Any())
            {
                throw new HotelCannotDeleteException("Cannot delete a hotel with available rooms.");
            }

            if (hotel.Bookings != null && hotel.Bookings.Any())
            {
                throw new HotelCannotDeleteException("Cannot delete a hotel with existing bookings.");
            }

            await _hotelRepository.DeleteAsync(hotel);
        }

        public async Task<IEnumerable<Room>> SearchForAvailableRoomsAsync(string name, DateTime startDate, DateTime endDate)
        {
            var hotel = await _hotelRepository.GetHotelByNameAsync(name);

            if (hotel == null) throw new HotelNotFoundException();

            return hotel.AvailableRooms.Where(i => i.IsAvailableForDates(startDate, endDate));
        }

        public async Task<Booking> AddBookingAsync(BookingRequest bookingRequest)
        {
            var room = await _roomRepository.GetByIdAsync(bookingRequest.RoomId);

            if (room == null) throw new RoomNotFoundException();

            if (!room.IsAvailableForDates(bookingRequest.StartDate, bookingRequest.EndDate))
            {
                throw new BookingInvalidException("Room is unavailable for booking dates.");
            }

            if (!room.IsSuitableForNumberOfGuests(bookingRequest.Guests.Count))
            {
                throw new BookingInvalidException("Room is not valid for number of guests.");
            }

            var booking = new Booking(room.HotelId, 
                room.Description, 
                bookingRequest.StartDate, 
                room.AvailableEndDate, 
                bookingRequest.Guests);

            await _bookingRepository.AddAsync(booking);

            return booking;
        }
    }
}
