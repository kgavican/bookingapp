﻿using System.Threading.Tasks;
using BookingApp.Hotels.AppCore.Entities;

namespace BookingApp.Hotels.AppCore.Interfaces
{
    public interface IHotelRepository : IAsyncRepository<Hotel>
    {
        Task<Hotel> GetHotelWithAvailableRoomsAndBookings(int id);

        Task<Hotel> GetHotelWithAvailableRooms(int id);

        Task<Hotel> GetHotelByNameAsync(string name);
    }
}
