﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BookingApp.Hotels.AppCore.Entities;

namespace BookingApp.Hotels.AppCore.Interfaces
{
    public interface IAsyncRepository<T> where T : BaseEntity
    {
        Task<T> GetByIdAsync(int id);

        Task<T> AddAsync(T item);

        Task<T> UpdateAsync(T item);

        Task DeleteAsync(T item);

        Task<IEnumerable<T>> ListAllAsync();
    }
}
