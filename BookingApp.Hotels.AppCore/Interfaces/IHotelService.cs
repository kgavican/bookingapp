﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BookingApp.Hotels.AppCore.Entities;
using BookingApp.Hotels.AppCore.Models;

namespace BookingApp.Hotels.AppCore.Interfaces
{
    public interface IHotelService
    {
        Task<Hotel> GetAsync(int id, bool includeAvailableRooms = false);

        Task DeleteAsync(int id);

        Task<IEnumerable<Room>> SearchForAvailableRoomsAsync(string name, DateTime startDate, DateTime endDate);

        Task<Booking> AddBookingAsync(BookingRequest bookingRequest);
    }
}
