﻿using System;
using System.Collections.Generic;
using BookingApp.Hotels.AppCore.Entities;

namespace BookingApp.Hotels.AppCore.Models
{
    public class BookingRequest
    {
        public int RoomId { get; }

        public List<Guest> Guests { get; }
        public DateTime StartDate { get; }
        public DateTime EndDate { get; }

        public BookingRequest(int roomId, DateTime startDate, DateTime endDate, List<Guest> guests)
        {
            RoomId = roomId;
            StartDate = startDate;
            EndDate = endDate;
            Guests = guests;
        }
    }
}
