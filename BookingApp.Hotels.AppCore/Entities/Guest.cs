﻿using System;

namespace BookingApp.Hotels.AppCore.Entities
{
    public class Guest : BaseEntity
    {
        public Guest()
        {
            // for EF
        }

        public string Name { get; private set; }
        public int Age { get; private set; }

        public int BookingId { get; private set; }
        public Booking Booking { get; private set; }

        public Guest(string name, int age)
        {
            Name = !string.IsNullOrEmpty(name) ? name : throw new NullReferenceException(nameof(name));
            Age = age;
        }
    }
}
