﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BookingApp.Hotels.AppCore.Entities
{
    public class Room : BaseEntity
    {
        public Room()
        {
            // For EF
        }

        public string Description { get; private set; }
        public DateTime AvailableStartDate { get; private set; }
        public DateTime AvailableEndDate { get; private set; }
        public int MaxGuests { get; private set; }

        public ICollection<string> Images { get; private set; } = new List<string>();

        public int HotelId { get; private set; }
        public Hotel Hotel { get; private set; }

        public Room(string description, List<string> images, 
            DateTime availableStartDate, DateTime availableEndDate, int maxGuests)
        {
            ValidateImages(images);
            ValidateDates(availableStartDate, availableEndDate);

            Description = !string.IsNullOrWhiteSpace(description) ? description : throw new ArgumentNullException(nameof(description));
            Images = images;
            AvailableStartDate = availableStartDate;
            AvailableEndDate = availableEndDate;
            MaxGuests = maxGuests;
        }

        private void ValidateImages(List<string> images)
        {
            if (images == null || !images.Any())
            {
                throw new ArgumentException(nameof(images));
            }
        }

        private void ValidateDates(DateTime startDate, DateTime endDate)
        {
            if (startDate > endDate)
            {
                throw new ArgumentException(nameof(endDate));
            }
        }

        public bool IsAvailableForDates(DateTime startDate, DateTime endDate)
        {
            return startDate >= AvailableStartDate && endDate <= AvailableEndDate;
        }

        public bool IsSuitableForNumberOfGuests(int numberOfGuests)
        {
            return numberOfGuests <= MaxGuests;
        }
    }
}
