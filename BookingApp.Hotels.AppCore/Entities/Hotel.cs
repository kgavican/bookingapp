﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BookingApp.Hotels.AppCore.Entities
{
    public class Hotel : BaseEntity
    {
        public Hotel()
        {
            // for EF
        }

        public string Name { get; private set; }
        public string Address { get; private set; }
        public string City { get; private set; }
        public string Phone { get; private set; }
        public int Stars { get; private set; }

        public ICollection<Room> AvailableRooms { get; } = new List<Room>();

        public ICollection<Booking> Bookings { get; } = new List<Booking>();

        public Hotel(string name, string address, string city, string phone, int stars, List<Room> availableRooms = null)
        {
            Name = !string.IsNullOrWhiteSpace(name) ? name : throw new ArgumentNullException(nameof(name));
            Address = !string.IsNullOrWhiteSpace(address) ? address : throw new ArgumentNullException(nameof(address));
            City = !string.IsNullOrWhiteSpace(city) ? city : throw new ArgumentNullException(nameof(city));
            Phone = !string.IsNullOrWhiteSpace(phone) ? phone : throw new ArgumentNullException(nameof(phone));
            Stars = stars;

            if(availableRooms!=null && availableRooms.Any())
            {
                AvailableRooms = availableRooms;
            }
        }
    }
}
