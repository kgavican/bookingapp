﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BookingApp.Hotels.AppCore.Entities
{
    public class Booking : BaseEntity
    {
        public Booking()
        {
            // for EF
        }

        public ICollection<Guest> Guests { get; private set; } = new List<Guest>();
        public DateTime StartDate { get; private set; }
        public DateTime EndDate { get; private set; }

        public string RoomDescription { get; private set; }

        public int HotelId { get; private set; }
        public Hotel Hotel { get; private set; }

        public Booking(int hotelId, string roomDescription, DateTime startDate, DateTime endDate, List<Guest> guests)
        {
            ValidateDates(startDate, endDate);
            ValidateGuests(guests);

            HotelId = hotelId;
            RoomDescription = !string.IsNullOrWhiteSpace(roomDescription) ? roomDescription : throw new ArgumentNullException(nameof(roomDescription));
            Guests = guests;
            StartDate = startDate;
            EndDate = endDate;
        }

        private void ValidateGuests(List<Guest> guests)
        {
            if (guests == null || !guests.Any())
            {
                throw new ArgumentException(nameof(guests));
            }
        }

        private void ValidateDates(DateTime startDate, DateTime endDate)
        {
            if (startDate > endDate)
            {
                throw new ArgumentException(nameof(startDate));
            }
        }
    }  
}
