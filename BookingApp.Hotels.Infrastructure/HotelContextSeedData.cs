﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BookingApp.Hotels.AppCore.Entities;

namespace BookingApp.Hotels.Infrastructure
{
    public class HotelContextSeedData
    {
        public static async Task SeedHotelData(HotelContext context)
        {
            context.Hotels.Add(new Hotel(
                "Ritz",
                "12 Grafton Street",
                "Dublin",
                "+35318224478",
                5,
                new List<Room>()
                {
                    new Room("Couples Room", new List<string>()
                        {
                            "http://somehost/someimage.jpg"
                        },
                        new DateTime(2018, 07, 01),
                        new DateTime(2018, 07, 08),
                        2),
                    new Room("Family Room", new List<string>()
                        {
                            "http://somehost/someimage.jpg"
                        },
                        new DateTime(2018, 07, 04),
                        new DateTime(2018, 07, 07),
                        5),
                }));

            await context.SaveChangesAsync();

        }
    }
}
