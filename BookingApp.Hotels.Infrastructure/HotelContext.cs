﻿using System;
using System.Collections.Generic;
using BookingApp.Hotels.AppCore.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BookingApp.Hotels.Infrastructure
{
    public class HotelContext : DbContext
    {
        public HotelContext(DbContextOptions<HotelContext> options) : base(options)
        {    
        }

        public DbSet<Hotel> Hotels { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<Guest> Guests { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Hotel>(ConfigureHotels);
            builder.Entity<Room>(ConfigureRooms);
            builder.Entity<Booking>(ConfigureBookings);
            builder.Entity<Guest>(ConfigureGuests);
        }

        private void ConfigureGuests(EntityTypeBuilder<Guest> obj)
        {
            obj.HasKey(i => i.Id);
            obj.Property(i => i.Id).IsRequired();

            obj.Property(i => i.Name).IsRequired();
            obj.Property(i => i.Age).IsRequired();
        }

        private void ConfigureBookings(EntityTypeBuilder<Booking> obj)
        {
            obj.HasKey(i => i.Id);
            obj.Property(i => i.Id).IsRequired();

            obj.Property(i => i.RoomDescription).IsRequired();

            obj.HasMany(i => i.Guests)
                .WithOne(i => i.Booking)
                .HasForeignKey(i => i.BookingId);
                
            obj.Property(i => i.StartDate).IsRequired();
            obj.Property(i => i.EndDate).IsRequired();
        }

        private void ConfigureRooms(EntityTypeBuilder<Room> obj)
        {
            obj.HasKey(i => i.Id);
            obj.Property(i => i.Id).IsRequired();

            obj.Property(i => i.Description).IsRequired();

            obj.Property(i => i.AvailableStartDate).IsRequired();
            obj.Property(i => i.AvailableEndDate).IsRequired();

            obj.Property(i => i.MaxGuests).IsRequired();

            // ToDo: Ignoring this mapping but maybe map to serialized JSON if had time
            obj.Ignore(i => i.Images);
        }


        private void ConfigureHotels(EntityTypeBuilder<Hotel> obj)
        {
            obj.HasKey(i => i.Id);
            obj.Property(i => i.Id).IsRequired();

            obj.Property(i => i.Name).IsRequired();
            obj.Property(i => i.Address).IsRequired();
            obj.Property(i => i.City).IsRequired();
            obj.Property(i => i.Phone).IsRequired();
            obj.Property(i => i.Stars).IsRequired();

            obj.HasMany(i => i.AvailableRooms)
                .WithOne(i => i.Hotel)
                .HasForeignKey(i => i.HotelId);

            obj.HasMany(i => i.Bookings)
                .WithOne(i => i.Hotel)
                .HasForeignKey(i => i.HotelId);
        }
    }
}

