﻿using System;
using System.Threading.Tasks;
using BookingApp.Hotels.AppCore.Entities;
using BookingApp.Hotels.AppCore.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BookingApp.Hotels.Infrastructure.Repositories
{
    public class HotelRepository : EntityRepository<Hotel>, IHotelRepository
    {
        public HotelRepository(HotelContext context) : base(context)
        {
            _context = context;
        }

        public async Task<Hotel> GetHotelWithAvailableRooms(int id)
        {
            return await _context.Hotels
                .Include(i => i.AvailableRooms)
                .FirstOrDefaultAsync(i => i.Id == id);
        }

        public async Task<Hotel> GetHotelWithAvailableRoomsAndBookings(int id)
        {
            return await _context.Hotels
                .Include(i => i.AvailableRooms)
                .Include(i => i.Bookings)
                .FirstOrDefaultAsync(i => i.Id == id);
        }

        public async Task<Hotel> GetHotelByNameAsync(string hotelName)
        {
            return await _context.Hotels
                .FirstOrDefaultAsync(i => string.Equals(i.Name, hotelName, StringComparison.InvariantCultureIgnoreCase) );
        }
    }
}
