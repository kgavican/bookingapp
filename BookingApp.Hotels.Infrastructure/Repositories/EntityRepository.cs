﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BookingApp.Hotels.AppCore.Entities;
using BookingApp.Hotels.AppCore.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BookingApp.Hotels.Infrastructure.Repositories
{
    public class EntityRepository<T> : IAsyncRepository<T> where T: BaseEntity
    {
        protected HotelContext _context;

        public EntityRepository(HotelContext context)
        {
            _context = context;
        }

        public async Task<T> GetByIdAsync(int id)
        {
            return await _context.Set<T>().FindAsync(id);
        }

        public async Task<T> AddAsync(T item)
        {
            await _context.Set<T>().AddAsync(item);
            await _context.SaveChangesAsync();
            return item;
        }

        public async Task<T> UpdateAsync(T item)
        {
             _context.Set<T>().Update(item);
            await _context.SaveChangesAsync();
            return item;
        }

        public async Task DeleteAsync(T item)
        {
            _context.Set<T>().Remove(item);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<T>> ListAllAsync()
        {
            return await _context.Set<T>().ToListAsync();
        }
    }
}
