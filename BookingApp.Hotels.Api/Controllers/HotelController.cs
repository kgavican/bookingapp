﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using BookingApp.Hotels.Api.Models;
using BookingApp.Hotels.AppCore.Exceptions;
using BookingApp.Hotels.AppCore.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace BookingApp.Hotels.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class HotelController : ControllerBase
    {
        private readonly IHotelService _hotelService;
        private readonly IHotelRepository _hotelRepository;

        public HotelController(IHotelService hotelService, 
            IHotelRepository hotelRepository)
        {
            _hotelService = hotelService;
            _hotelRepository = hotelRepository;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Hotel>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> Get()
        {
            var entities = await _hotelRepository.ListAllAsync();

            return Ok(entities);
        }

        [HttpGet("FindRoom")]
        [ProducesResponseType(typeof(RoomSearchResult), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> FindRoom([FromQuery]string name, [FromQuery]DateTime from, [FromQuery]DateTime until)
        {
            var result = new RoomSearchResult();

            try
            {
                var entities = (await _hotelService.SearchForAvailableRoomsAsync(name, from, until)).ToList();

                result.Results = entities.Select(i => new Room(i)).ToList();
                result.NumOfResults = entities.Count;     
            }
            catch (HotelNotFoundException)
            {
                result.Message = $"No hotel '{name}' found.";
            }

            return Ok(result);
        }

        [HttpGet("{id}", Name = "Get")]
        [ProducesResponseType(typeof(Hotel), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> GetById(int id, [FromQuery]bool includeRooms=false)
        {

            var entity = await _hotelService.GetAsync(id, includeRooms);

            if (entity == null)
            {
                return NotFound();
            }

            return Ok(new Models.Hotel(entity));
        }

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Register([FromBody] Hotel hotel)
        {
            var newHotel = await _hotelRepository.AddAsync(hotel.MapToEntity());

            return CreatedAtAction(nameof(GetById), new { id = newHotel.Id }, null);
        }

        [HttpPost("{id}/Reservation")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<IActionResult> AddBooking(int id, [FromBody] BookingRequest bookingRequest)
        {
            try
            {
                var booking = await _hotelService.AddBookingAsync(bookingRequest.MapToEntity());

                return CreatedAtAction(nameof(GetBooking), new {id = booking.HotelId, bookingId = booking.Id}, null);
            }
            catch (BookingInvalidException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (RoomNotFoundException)
            {
                return NotFound();
            }
        }

        [HttpGet("{id}/Reservation/{bookingId}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public IActionResult GetBooking(int id, int bookingId)
        {
             throw new NotImplementedException();
        }


        [HttpPut("{id}")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<IActionResult> Update(int id, [FromBody] Hotel hotel)
        {
            var newHotel = await _hotelRepository.AddAsync(hotel.MapToEntity());

            return CreatedAtAction(nameof(GetById), new { id = newHotel.Id }, null);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _hotelService.DeleteAsync(id);
            }
            catch (HotelCannotDeleteException ex)
            {
                return BadRequest(ex.Message);
            }

            return NoContent();
        }
    }
}
