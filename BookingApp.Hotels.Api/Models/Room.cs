﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BookingApp.Hotels.Api.Models
{
    public class Room
    {
        public Room() { }

        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime AvailableStartDate { get; set; }
        public DateTime AvailableEndDate { get; set; }
        public int MaxGuests { get; set; }

        public List<string> Images { get; set; } = new List<string>();

        public int HotelId { get; set; }

        public Room(AppCore.Entities.Room entity)
        {
            Id = entity.Id;
            Description = entity.Description;
            AvailableStartDate = entity.AvailableStartDate;
            AvailableEndDate = entity.AvailableEndDate;
            MaxGuests = entity.MaxGuests;
            Images = entity.Images.ToList();
            HotelId = entity.HotelId;
        }

        public AppCore.Entities.Room MapToEntity()
        {
            return new AppCore.Entities.Room(Description, Images, AvailableStartDate, AvailableEndDate, MaxGuests);
        }
    }
}
