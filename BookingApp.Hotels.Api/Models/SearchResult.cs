﻿using System.Collections.Generic;

namespace BookingApp.Hotels.Api.Models
{
    public class RoomSearchResult
    {
        public string Message { get; set; }
        public int NumOfResults { get; set; }
        public List<Room> Results { get; set; } = new List<Room>();
    }
}
