﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BookingApp.Hotels.Api.Models
{
    public class Booking
    {
        public Booking() { }

        public int Id { get; set; }
        public List<Guest> Guests { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string RoomDescription { get; set; }
        public int HotelId { get; set; }

        public Booking(AppCore.Entities.Booking entity)
        {
            Id = entity.Id;
            Guests = entity.Guests.Select(i => new Guest(i)).ToList();
            StartDate = entity.StartDate;
            EndDate = entity.EndDate;
            RoomDescription = entity.RoomDescription;
            HotelId = entity.HotelId;
        }

        public AppCore.Entities.Booking MapToEntity()
        {
            return new AppCore.Entities.Booking(HotelId, RoomDescription, StartDate, EndDate, Guests.Select(i => i.MapToEntity()).ToList());
        }
    }  
}
