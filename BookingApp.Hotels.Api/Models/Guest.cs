﻿namespace BookingApp.Hotels.Api.Models
{
    public class Guest
    {
        public Guest() { }

        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }

        public Guest(AppCore.Entities.Guest guest)
        {
            Id = guest.Id;
            Name = guest.Name;
            Age = guest.Age;
        }

        public AppCore.Entities.Guest MapToEntity()
        {
            return new AppCore.Entities.Guest(Name, Age);
        }
    }
}
