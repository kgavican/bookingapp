﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BookingApp.Hotels.Api.Models
{
    public class BookingRequest
    {
        public BookingRequest() { }

        public int RoomId { get; set; }
        public List<Guest> Guests { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public BookingRequest(int roomId, DateTime startDate, DateTime endDate, List<Guest> guests)
        {
            RoomId = roomId;
            StartDate = startDate;
            EndDate = endDate;
            Guests = guests;
        }

        public AppCore.Models.BookingRequest MapToEntity()
        {
            return new AppCore.Models.BookingRequest(RoomId, StartDate, EndDate,
                Guests.Select(i => i.MapToEntity()).ToList());
        }
    }
}
