﻿using System.Collections.Generic;
using System.Linq;

namespace BookingApp.Hotels.Api.Models
{
    public class Hotel
    {
        public Hotel() { }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Phone { get; set; }
        public int Stars { get; set; }
        public List<Room> AvailableRooms { get; set; }
        public List<Booking> Bookings { get; set; }

        public Hotel(AppCore.Entities.Hotel entity)
        {
            Id = entity.Id;
            Name = entity.Name;
            Address = entity.Address;
            City = entity.City;
            Phone = entity.Phone;
            Stars = entity.Stars;
            AvailableRooms = entity.AvailableRooms.Select(i => new Room(i)).ToList();
            Bookings = entity.Bookings.Select(i => new Booking(i)).ToList();
        }

        public AppCore.Entities.Hotel MapToEntity()
        {
            return new AppCore.Entities.Hotel(Name, Address, City, Phone, Stars,
                AvailableRooms.Select(i => i.MapToEntity()).ToList());
        }
    }
}
